# PHP Websockets Sandstone

Docker version of a Sandstone application.


## Installation

Clone this repo:

``` bash
git clone https://bitbucket.org/zareba_pawel/php-websockets-sandstone.git
cd php-websockets-sandstone/
```

Install Composer dependencies:

``` bash
cd src/
composer install
```


## Mount application with Docker

### Create and run container with Docker composer

`docker-compose up`

It starts php-fpm and nginx server.


### Start websocket server

In another thread, run:

`docker exec -ti php /bin/bash -c 'php bin/websocket-server.php'`

This will start push and websocket server.


### Open index-bob.html in the browser

`file:///path/to/php-websockets-sandstone/src/web/index-bob.html`

Then opens Javascript console and check logs.

It will automatically subscribes to chat topic,
and publish a Hello message to that same topic
(refresh the page if you missed it).


### Post message to the bob channel

Once websocket and Push servers are running,
and you have a client connected on the chat topic,
what you can experiment is to send a Push message from the RestApi to the chat topic.

In this example, we can use the RestApi endpoint `api/message/bob`
to push a message to the chat topic.

Just call `POST api/message/bob` with curl:

`curl -X POST http://127.0.0.1:8080/app_dev.php/api/message/bob -d '{"message":"Hello World!"}' -H 'Content-Type: application/json' -w "\n"`

You can check websocket server logs for the push event,
and check html page which will display this type of event.

It can for exemple send a real-time notification to subscribing client when RestApi state changed.


## Based on example from

`https://eole-io.github.io/sandstone/examples/full.html`