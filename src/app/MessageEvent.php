<?php

use Symfony\Component\EventDispatcher\Event;

class MessageEvent extends Event
{
    public $id;
    public $message;
    public $channel;

    private function setId($id)
    {
        $this->id = $id;
    }

    public function __construct()
    {
        $this->setId(rand());
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getChannel()
    {
        return $this->channel;
    }

    public function setChannel($channel)
    {
        $this->channel = sprintf('chat/%s', $channel);
        return $this;
    }


}
