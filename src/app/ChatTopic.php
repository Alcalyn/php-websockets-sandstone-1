<?php

use Ratchet\Wamp\WampConnection;
use Eole\Sandstone\Websocket\Topic;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ChatTopic extends Topic implements EventSubscriberInterface
{
    private $topic_path;

    public function __construct($topic_path, array $arguments = [])
    {
        parent::__construct($topic_path, $arguments);

        $this->topic_path = $topic_path;
    }

    public static function getSubscribedEvents()
    {
        return [
            'message.created' => 'onMessageCreated',
        ];
    }

    public function onMessageCreated(MessageEvent $event)
    {
        if (0 === strcmp($event->getChannel(), $this->topic_path)) {
            $this->broadcast([
                'type' => 'message_created',
                'message' => $event->getMessage()
            ]);
        }
    }

    public function onPublish(WampConnection $connection, $topic, $event)
    {
        $this->broadcast([
            'type' => 'message',
            'message' => $event,
        ]);
    }

    public function onSubscribe(WampConnection $connection, $topic)
    {
        parent::onSubscribe($connection, $topic);

        $this->broadcast([
            'type' => 'join',
            'message' => 'Someone has joined this channel.',
        ]);
    }

    public function onUnSubscribe(WampConnection $connection, $topic)
    {
        parent::onUnSubscribe($connection, $topic);

        $this->broadcast([
            'type' => 'leave',
            'message' => 'Someone has left this channel.',
        ]);
    }
}